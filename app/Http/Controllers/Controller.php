<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Auth;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * Is admin authenticated
     *
     * @return bool
     */
    public function admin_check()
    {
        // На клавиатуре не работает знак амперсанда :)
        return (Auth::check() and Auth::user()->name == 'admin');
    }
}
