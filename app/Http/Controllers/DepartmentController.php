<?php

namespace App\Http\Controllers;

use App\Department;
use App\Http\Requests\DepartmentRequest;
use App\Repositories\Department\DepartmentRepositoryInterface;
use App\User;
use Illuminate\Support\Facades\Log;

class DepartmentController extends Controller
{
    protected $departmentRepository;

    public function __construct(DepartmentRepositoryInterface $departmentRepository)
    {
        $this->departmentRepository = $departmentRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $departments = Department::with('users')->paginate(4);

        return view('departments.index', compact('departments'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (!$this->admin_check()) {
            return view('403');
        }

        $users = User::all();

        return view('departments.create', compact('users'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param DepartmentRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(DepartmentRequest $request)
    {
        if (!$this->admin_check()) {
            return response()->json([], 403);
        }

        $this->departmentRepository->store($request->validated());

        return response()->json([], 201);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Department $department
     * @return \Illuminate\Http\Response
     */
    public function edit(Department $department)
    {
        if (!$this->admin_check()) {
            return view('403');
        }

        $users = User::all();

        return view('departments.edit', compact('department', 'users'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param DepartmentRequest $request
     * @param Department $department
     * @return \Illuminate\Http\Response
     */
    public function update(DepartmentRequest $request, Department $department)
    {
        if (!$this->admin_check()) {
            return response()->json([], 403);
        }

        $this->departmentRepository->update($department, $request->validated());

        return response()->json([], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Department $department
     * @return \Illuminate\Http\Response
     */
    public function destroy(Department $department)
    {
        if (!$this->admin_check()) {
            return response()->json([], 403);
        }

        try {
            $department->delete();

            return response()->json([], 200);
        } catch (\Exception $e) {
            Log::error("Department with id {{$department->id}} cannot deleted");
            return response()->json([], 500);
        }
    }
}
