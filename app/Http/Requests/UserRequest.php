<?php
namespace App\Http\Requests;


use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'  => 'string|required',
            'email' => 'email|required',
            'password' => 'string|required',
        ];
    }

    /**
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public function getValidatorInstance()
    {
        $this->checkPassword();

        return parent::getValidatorInstance();
    }

    /**
     * Check and encrypt password
     */
    protected function checkPassword()
    {
        if($this->request->has('password')){
            $this->merge([
                'password' => bcrypt($this->request->get('password'))
            ]);
        }
    }
}