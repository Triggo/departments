<?php

namespace App\Providers;

use App\Services\Images\ImageService;
use Illuminate\Support\ServiceProvider;

class UploadServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'App\Services\Images\ImageServiceInterface', function () {
            return new ImageService();
        });
    }
}
