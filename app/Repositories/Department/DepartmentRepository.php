<?php
namespace App\Repositories\Department;


use App\Department;
use App\Services\Images\ImageServiceInterface;

class DepartmentRepository implements DepartmentRepositoryInterface
{
    protected $department;
    protected $imageService;
    protected $uploadPath = '';
    protected $settings = [];

    public function __construct(Department $department, ImageServiceInterface $imageService)
    {
        $this->department = $department;
        $this->imageService = $imageService;
    }

    public function store(array $validatedData)
    {
        $department = $this->department->create($validatedData);

        foreach ($validatedData['users'] as $user) {
            $department->users()->attach($user);
        }

        if (isset($validatedData['logo'])) {
            $fileName = $this->imageService->uploadFiles($this->uploadPath, $validatedData['logo'], $this->settings);
            $department->logo = $fileName;
        }

        $department->save();

        return $department;
    }

    public function update(Department $department, array $validatedData)
    {
        $department->update($validatedData);

        $usersIds = $validatedData['users'];

        $existsUsersIds = $department->users->pluck('id')->toArray();

        $newUsersIds = [];

        foreach ($usersIds as $userId) {
            if (!in_array($userId, $existsUsersIds)) {
                array_push($newUsersIds, $userId);
            }
        }

        $toDeleteUsersIds = [];

        foreach ($existsUsersIds as $userId) {
            if (!in_array($userId, $usersIds)) {
                array_push($toDeleteUsersIds, $userId);
            }
        }

        $department->users()->attach($newUsersIds);
        $department->users()->detach($toDeleteUsersIds);

        if (isset($validatedData['logo'])) {
            $fileName = $this->imageService->uploadFiles($this->uploadPath, $validatedData['logo'], $this->settings);
            $department->logo = $fileName;
        }

        $department->save();
    }
}