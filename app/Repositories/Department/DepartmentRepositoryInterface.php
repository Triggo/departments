<?php
namespace App\Repositories\Department;


use App\Department;

interface DepartmentRepositoryInterface
{
    public function store(array $validatedData);
    public function update(Department $department, array $validatedData);
}