<?php
namespace App\Repositories\Department;


use Illuminate\Support\ServiceProvider;

class DepartmentServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'App\Repositories\Department\DepartmentRepositoryInterface',
            'App\Repositories\Department\DepartmentRepository'
        );
    }
}