<?php
namespace App\Repositories\User;


use App\User;

class UserRepository implements UserRepositoryInterface
{
    protected $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function store(array $validatedData)
    {
        return $this->user->create($validatedData);
    }

    public function update(User $user, array $validatedData)
    {
        return $user->update($validatedData);
    }
}