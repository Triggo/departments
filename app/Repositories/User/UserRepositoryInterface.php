<?php
namespace App\Repositories\User;


use App\User;

interface UserRepositoryInterface
{
    public function store(array $validatedData);
    public function update(User $user, array $validatedData);
}