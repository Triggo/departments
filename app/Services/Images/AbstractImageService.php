<?php

namespace App\Services\Images;


abstract class AbstractImageService implements ImageServiceInterface
{
    abstract public function uploadFiles(string $uploadPath, $file, array $settings);
}