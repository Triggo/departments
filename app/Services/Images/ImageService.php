<?php

namespace App\Services\Images;


use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class ImageService extends AbstractImageService
{
    public function uploadFiles(string $uploadPath, $image, array $settings)
    {
            $fileName   = time() . '.' . $image->getClientOriginalExtension();

            $img = Image::make($image->getRealPath());
            $img->resize(100, null, function ($constraint) {
                $constraint->aspectRatio();
            });

            $img->stream(); // <-- Key point

            Storage::disk('local')->put('logo'.'/'.$fileName, $img, 'public');

            return $fileName;
    }
}