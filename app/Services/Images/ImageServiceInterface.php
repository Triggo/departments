<?php
namespace App\Services\Images;


interface ImageServiceInterface
{
    public function uploadFiles(string $uploadPath, $file, array $settings);
}