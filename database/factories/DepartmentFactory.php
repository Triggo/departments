<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Department;
use Faker\Generator as Faker;

$factory->define(Department::class, function (Faker $faker) {
    return [
        'name' => $faker->company,
        'description' => $faker->text,
        'logo' => $faker->image('storage/app/logo', 100, 50, null, false),
    ];
});
