<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DepartmentsAndUsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'admin',
            'email' => 'admin@test.loc',
            'password' => bcrypt('password'),
        ]);

        $departments = factory(App\Department::class, 15)->create();
        $users = factory(App\User::class, 15)->create();

        $users->each(function (App\User $u) use ($departments) {
            $u->departments()->attach(
                $departments->random(rand(5, 10))->pluck('id')->toArray()
            );
    });
    }
}
