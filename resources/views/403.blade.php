@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    403. You don't have permissions
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
