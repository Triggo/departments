@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        Department creating form
                    </div>

                    <div class="card-body">

                        <form name="departmentForm" method="post" enctype="multipart/form-data">
                            <div class="form-group">
                                <label>Name</label>
                                <input type="text" name="name" placeholder="Enter name" class="form-control"
                                       value="@if(isset($department->name)){{$department->name}}@endif">
                            </div>
                            <div class="form-group">
                                <label>Text</label>
                                <textarea name="description" placeholder="Enter description" class="form-control"
                                          rows="5">
                                    @if(isset($department->description)){{$department->description}}@endif
                                </textarea>
                            </div>
                            <div class="custom-file">
                                <input type="file" name="logo" class="custom-file-input" id="validatedCustomFile">
                                <label class="custom-file-label" for="validatedCustomFile">Select file</label>
                            </div>

                            <div class="form-group mt-3">
                                @foreach($users as $user)
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="users[]" value="{{$user->id}}">
                                            {{$user->name}} ({{$user->email}})
                                        </label>
                                    </div>
                                @endforeach
                            </div>

                            <button type="submit" class="btn btn-info">Send</button>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('javascripts')
    <script>
        $(document).ready(function () {
            let departmentForm = $('[name="departmentForm"]');

            departmentForm.submit(function (e) {
                let formData = new FormData(this);
                e.preventDefault();

                $.ajax({
                    type: 'post',
                    url: "/departments",
                    data: formData,
                    mimeType: "multipart/form-data",
                    contentType: false,
                    cache: false,
                    processData: false,
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    complete: function(e, xhr, settings){
                        if(e.status === 201){
                            alert('Department successfully created');
                            window.location.href = '/departments';
                        } else if(e.status === 404) {
                            alert('You don`t have permissions');
                        } else{
                            alert('Something went wrong');
                        }
                    }
                });
            });
        });
    </script>
@endsection
