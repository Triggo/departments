@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    Departments
                    <a href="/departments/create" type="button" class="btn btn-primary float-md-right">Add</a>
                </div>

                <div class="card-body">

                    <table class="table table-hover">
                        <tbody>
                        @foreach($departments as $department)
                        <tr>
                            <td>
                                <img src="/logo/{{$department->logo}}">
                            </td>
                            <td style="width: 50%">
                                <strong>{{$department->name}}</strong>
                                <p>{{$department->description}}</p>
                            </td>
                            <td>
                                <strong>Users:</strong>
                                @foreach($department->users as $user)
                                    <div>{{$user->name}}</div>
                                @endforeach
                            </td>
                            <td>
                                <a type="button" class="btn btn-secondary" href="/departments/{{$department->id}}/edit">Edit</a>
                                <button type="button" class="btn btn-danger delete" data-id="{{$department->id}}">Delete</button>
                            </td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{ $departments->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('javascripts')
<script>
    $(document).ready(function() {
        $('.delete').click(function() {
            if (confirm('Вы действительно хотите удалить отдел?')) {
                $.ajax({
                    type: 'delete',
                    url: "/departments/" + $(this).data('id'),
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    complete: function(e, xhr, settings){
                        if(e.status === 200){
                            alert('Department successfully deleted');
                            location.reload();
                        } else if(e.status === 404) {
                            alert('You don`t have permissions');
                        } else {
                            alert('Something went wrong');
                        }
                    }
                });
            }
        });
    });
</script>
@endsection
