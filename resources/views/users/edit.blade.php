@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    {{$user->name}} editing
                    <a href="/users/create" type="button" class="btn btn-primary float-md-right">Add</a>
                </div>

                <div class="card-body">

                        <form name="userForm" method="post">
                            <div class="form-group">
                                <label>Name</label>
                                <input type="text" name="name" placeholder="Enter name" class="form-control" value="@if(isset($user->name)){{$user->name}}@endif">
                            </div>
                            <div class="form-group">
                                <label>E-mail</label>
                                <input type="text" name="email" placeholder="Enter email" class="form-control" value="@if(isset($user->email)){{$user->email}}@endif">
                            </div>
                            <div class="form-group">
                                <label>Password</label>
                                <input type="password" name="password" placeholder="Enter password" class="form-control" value="@if(isset($user->password)){{$user->password}}@endif">
                            </div>
                            <button type="submit" class="btn btn-info">Send</button>
                        </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('javascripts')
<script>
    $(document).ready(function() {
        let userForm = $('[name="userForm"]');

        userForm.submit(function (e) {
            e.preventDefault();

            $.ajax({
                type: 'put',
                url: "/users/{{$user->id}}",
                data: userForm.serialize(),
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                complete: function(e, xhr, settings){
                    if(e.status === 200){
                        alert('User successfully updated');
                        location.reload();
                    } else if(e.status === 404) {
                        alert('You don`t have permissions');
                    }else {
                        alert('Something went wrong');
                    }
                }
            });
        });
    });
</script>
@endsection
