@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    Users
                    <a href="/users/create" type="button" class="btn btn-primary float-md-right">Add</a>
                </div>

                <div class="card-body">

                    <table class="table table-hover">
                        <tbody>
                        @foreach($users as $user)
                        <tr>
                            <td>{{$user->name}}</td>
                            <td>{{$user->email}}</td>
                            <td>{{$user->created_at}}</td>
                            <td>
                                <a type="button" class="btn btn-secondary" href="/users/{{$user->id}}/edit">Edit</a>
                                <button type="button" class="btn btn-danger delete" data-id="{{$user->id}}">Delete</button>
                            </td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{ $users->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('javascripts')
<script>
    $(document).ready(function() {
        $('.delete').click(function() {
            if (confirm('Вы действительно хотите удалить пользователя?')) {
                $.ajax({
                    type: 'delete',
                    url: "/users/" + $(this).data('id'),
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    complete: function(e, xhr, settings){
                        if(e.status === 200){
                            alert('User successfully deleted');
                            location.reload();
                        } else if(e.status === 404) {
                            alert('You don`t have permissions');
                        }else {
                            alert('Something went wrong');
                        }
                    }
                });
            }
        });
    });
</script>
@endsection
