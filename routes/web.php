<?php

Route::get('/', 'DepartmentController@index');

Route::resource('/users', 'UserController');
Route::resource('/departments', 'DepartmentController');

Auth::routes();