<?php

namespace Tests\Feature;

use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserTest extends TestCase
{
    /**
     * @test
     */
    public function if_user_not_authenticated_he_redirect_to_login_page()
    {
        $response = $this->get('/departments/1/edit');
        $response->assertViewIs('403');
    }

    /** @test */
    public function user_cannot_view_a_login_form_when_authenticated()
    {
        $user = factory(User::class)->make();

        $response = $this->actingAs($user)->get('/login/');
        $response->assertRedirect('/departments');
    }

    /** @test */
    public function user_can_login_with_correct_credentials()
    {
        $this->withoutMiddleware();

        $user = factory(User::class)->create([
            'password' => bcrypt($password = 'i-love-laravel'),
        ]);

        $response = $this->post('/login', [
            'email' => $user->email,
            'password' => $password,
        ]);
        $this->assertAuthenticatedAs($user);
    }
}
